Pod::Spec.new do |s|
  s.name         = "PHMessaging"
  s.version      = "0.3.5"
  s.platform     = :ios, '7.1'
  s.summary      = "PHMessaging. XMPP chat framework!"
  s.homepage     = "https://bitbucket.org/philology/phmessaging"
  s.license      = {
     :type => 'Philology',
     :text => <<-LICENSE
              Copyright (C) 2013 Philology Pty. Ltd.

              All rights reserved.
     LICENSE
  }
  s.author       = { "Ashton Williams" => "Ashton.Williams@philology.com.au", "Ricol Wang" => "Ricol.Wang@philology.com.au" }
  s.source       = { :git => "https://RicolWang@bitbucket.org/philology/phmessaging.git", :tag => s.version.to_s }
  s.source_files = 'PHMessaging/Classes', 'PHMessaging/Classes/**/*.{h,m}'
  s.resources    = 'PHMessaging/Classes/*.storyboard', 'PHMessaging/Resources/*.png', 'PHMessaging/Classes/*.lproj', 'PHMessaging/Classes/*.xib'
  s.frameworks   = 'CoreData', 'CoreLocation', 'Security', 'CFNetwork', 'SystemConfiguration'
  s.libraries    = 'xml2', 'resolv'
  s.xcconfig     = {
 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2 $(PODS_ROOT)/XMPPFramework/module',
'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
'OTHER_LDFLAGS' => '"$(inherited)" "-lxml2" "-objc"'
}
  s.requires_arc = true
  s.dependency 'JSQSystemSoundPlayer', '1.5.2'
  s.dependency 'JSMessagesViewController', '4.0.4'
  s.dependency 'CocoaLumberjack', '<=2.0.0-beta3'
  s.dependency 'CocoaAsyncSocket'
  s.dependency 'XMPPFramework', '3.6.6'
  s.dependency 'PHCommon', '>= 0.4.2'
end

