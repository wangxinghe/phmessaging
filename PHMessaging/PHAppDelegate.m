//
//  PHAppDelegate.m
//  PHMessaging
//
//  Created by Philology on 4/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHAppDelegate.h"
#import "TestViewController.h"
#import "PHMessagingManager.h"
#import "PHMessagingSplitViewController.h"

#define IP @"192.168.0.11"
#define NAME @"alison"

@interface PHAppDelegate ()

@end

@implementation PHAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Document: %@", DOCUMENTS_DIRECTORY);
    
    TestViewController *tmpVC = [[TestViewController alloc] init];
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    
    [PHMessagingManager sharedPHMessagingManager].dataSourceForContactName = tmpVC;
    [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar = tmpVC;
    [PHMessagingManager sharedPHMessagingManager].delegateForContactVC = tmpVC;
    
    
    NSString *myJID = [NSString stringWithFormat:@"%@@%@", NAME, IP];
    NSString *myPassword = @"cookie";
    // Setup Messaging
    NSLog(@"AppDelegate->userDidLogin:Connecting to Messaging Server with Account: %@ and Password: %@", myJID, myPassword);

    if ([[PHMessagingManager sharedPHMessagingManager] connectWithUser:myJID password:myPassword])
    {
        NSLog(@"AppDelegate->userDidLogin:Connection to Messaging Server Succeed.");
    }else
    {
        NSLog(@"AppDelegate->userDidLogin:Error! Connection to Messaging Server Failed!");;
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    UINavigationController *tmpNav = [[UINavigationController alloc] initWithRootViewController:tmpVC];
    tmpNav.navigationBarHidden = YES;
    self.window.rootViewController = tmpNav;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
