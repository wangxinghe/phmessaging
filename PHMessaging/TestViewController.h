//
//  TestViewController.h
//  PHMessaging
//
//  Created by Ricol Wang on 1/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicViewController.h"
#import "PHMessagingSplitViewController.h"
#import "PHAppDelegate.h"
#import "PHMessagingContactNameDataSource.h"

@interface TestViewController : PHBasicViewController <PHMessagingMainVCDelegate, PHMessagingContactNameDataSource, PHMessagingAvatarDatasource, PHContactsVCDelegate>

@end
