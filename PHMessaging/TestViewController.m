//
//  TestViewController.m
//  PHMessaging
//
//  Created by Ricol Wang on 1/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "TestViewController.h"
#import "PHMiniConversationViewController.h"
#import "PHMessagingManager.h"

@interface TestViewController () <NSFetchedResultsControllerDelegate>

@property (strong) PHMiniConversationViewController *miniVC;
@property (strong) UINavigationController *nav;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UITextView *tvText;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (strong) NSFetchedResultsController *fetchedResultsController;

- (IBAction)btnChatOnTapped:(id)sender;
- (IBAction)btnMiniChatOnTapped:(id)sender;
- (IBAction)btnShowListOfUsers:(id)sender;
- (IBAction)btnConvertOnTapped:(id)sender;

@end

@implementation TestViewController

@synthesize mutableDictAvatars;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mutableDictAvatars = [NSMutableDictionary new];
}

#pragma mark - PHMessagingMainVCDelegate

- (void)PHMessagingMainVCQuit:(PHMessagingSplitViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PHMessagingContactNameDataSource

- (NSString *)PHMessagingContactNameReplace:(NSString *)nameOriginal
{
    return nameOriginal;
    NSDictionary *tmpDict = @{
                              @"Device1" : @"",
                              @"Device2" : @"",
                              @"Device3" : @"",
                              @"Device4" : @"",
                              @"Device5" : @"",
                              @"Device6" : @"",
                              @"Device7" : @"Ricol Wang",
                              @"Device8" : @"Philip Cookson",
                              @"Device9" : @"Brenton Gill",
                              @"Device10" : @"Kia Cookson",
                              @"Device11" : @"Andew Milton",
                              @"Device12" : @"",
                              @"Device13" : @"",
                              @"Device14" : @"",
                              @"Device15" : @"",
                              @"Device16" : @"",
                              @"Device17" : @"",
                              @"Device18" : @"",
                              @"Device19" : @"",
                              @"Device20" : @""
                              };
    return [tmpDict[nameOriginal] isEqualToString:@""] ? nameOriginal : tmpDict[nameOriginal];
}

- (BOOL)PHMessagingContactNameShouldDisplay:(NSString *)nameOriginal
{
    return YES;
    NSString *tmpStrName = [self PHMessagingContactNameReplace:nameOriginal];
    return [[tmpStrName uppercaseString] rangeOfString:@"DEVICE"].location == NSNotFound;
}

- (IBAction)btnChatOnTapped:(id)sender
{
    UIViewController *tmpVC = [PHMessagingSplitViewController launchChattingWithDelegate:self];
    [self presentViewController:tmpVC animated:YES completion:nil];
}

- (IBAction)btnMiniChatOnTapped:(id)sender
{
    self.miniVC = [[PHMiniConversationViewController alloc] init];
    CGRect tmpRect = self.miniVC.view.frame;
    tmpRect = CGRectInset(tmpRect, 70, 50);
    self.miniVC.view.frame = tmpRect;
    
    self.nav = [[UINavigationController alloc] initWithRootViewController:self.miniVC];
    [self addChildViewController:self.nav];
    self.nav.view.frame = self.miniVC.view.frame;
    
    [self AddTheModelView:self.nav.view withStyle:DOWN_TO_CENTRE andComplete:^(){
        self.miniVC.targetUser = [PHMessagingManager getConversationUserFromUserId:@"ricol"];
    }];
}

- (IBAction)btnShowListOfUsers:(id)sender
{
//    NSLog(@"User: %@", [PHMessagingManager getConversationUserFromUserId:@"device10"]);
    
    NSManagedObjectContext *moc = [[PHMessagingManager sharedPHMessagingManager] managedObjectContext_roster];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    
    NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"sectionNum" ascending:YES];
    NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"displayName" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:moc
                                                                          sectionNameKeyPath:@"sectionNum"
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error])
    {
        NSLog(@"Error performing fetch: %@", error);
    }else
        NSLog(@"fectched record: %d\n%@", self.fetchedResultsController.fetchedObjects.count, self.fetchedResultsController.fetchedObjects);
}

- (IBAction)btnConvertOnTapped:(id)sender
{
    self.lblText.text = [self.tvText.text copy];
    
    NSString *tmpStr = self.lblText.text;
    CGSize tmpSize = self.viewContainer.bounds.size;
    UIFont *tmpFont = self.lblText.font;
    
    CGRect tmpRect = [tmpStr boundingRectWithSize:tmpSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : tmpFont} context:nil];
    
    CGSize stringSize = [tmpStr sizeWithFont:tmpFont constrainedToSize:tmpSize];
    tmpRect.size = stringSize;
    
    self.lblText.frame = tmpRect;
    
    [self printRect:tmpRect];
}

//+ (CGSize)neededSizeForText(NSString *)text
//{
//    CGFloat maxWidth =
//}

- (void)printRect:(CGRect)rect
{
    NSLog(@"rect: %f, %f, %f, %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

- (void)btnModalViewBackgroundOnTapped:(id)sender
{
    [self RemoveTheModelView:self.nav.view withStyle:CENTRE_TO_DOWN andComplete:nil];
    [self.nav removeFromParentViewController];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPat
{
    NSLog(@"did change object...");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSLog(@"did change section...");
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    NSLog(@"will change content...");
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSLog(@"did change content...");
}

#pragma mark - PHContactsVCDelegate

- (void)PHContactsVCQuit:(PHContactsViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
