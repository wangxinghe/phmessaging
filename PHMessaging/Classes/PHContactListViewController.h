//
//  PHContactListViewController.h
//  PHMessaging
//
//  Created by Ricol Wang on 20/03/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHContactsViewController.h"
#import "PHMessaging.h"

@interface PHContactListViewController : PHContactsViewController

- (void)selectUser:(NSArray *)users;

@end
