//
//  PHContactsViewController.h
//  PHMessaging
//
//  Created by Philology on 4/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CocoaLumberjack/DDLog.h>
#import "PHMessaging.h"
#import "PHMessagingAvatarDatasource.h"
#import "PHContactsVCDelegate.h"

@class PHConversationViewController;

@interface PHContactsViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

- (instancetype)initWithConversation:(PHConversationViewController *)conversation;

@end
