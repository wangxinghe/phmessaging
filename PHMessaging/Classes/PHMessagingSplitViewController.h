//
//  PHMessagingSplitViewController.h
//  PHMessaging
//
//  Created by Philology on 15/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHMessagingMainVCDelegate.h"
#import "PHMessaging.h"
#import "PHContactsViewController.h"
#import "PHConversationViewController.h"
#import "PHMessagingAvatarDatasource.h"

@interface PHMessagingSplitViewController : UISplitViewController

+ (UIViewController *)launchChattingWithDelegate:(id <PHMessagingMainVCDelegate>)delegate;

@end
