//
//  PHMessaging.h
//  PHMessaging
//
//  Created by Ricol Wang on 16/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#ifndef PHMessaging_PHMessaging_h
#define PHMessaging_PHMessaging_h

//#define _DEBUG
#define PHMessagingString(s) NSLocalizedStringFromTable(s, @"PHMessaging", nil)
#define KEY_MESSAGE_LIST_TITLE @"keyMessageListTitle"
#define KEY_MESSAGE_LIST_VALUE @"keyMessageListValue"

#endif
