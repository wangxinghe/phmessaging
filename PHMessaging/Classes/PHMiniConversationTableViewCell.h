//
//  PHMiniConversationTableViewCell.h
//  PHMessaging
//
//  Created by Ricol Wang on 1/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicConversationTableViewCellMessage.h"

@interface PHMiniConversationTableViewCell : PHBasicConversationTableViewCellMessage

@end
