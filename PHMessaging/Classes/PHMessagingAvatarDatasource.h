//
//  PHMessagingAvatarDatasource.h
//  PHMessaging
//
//  Created by Ricol Wang on 2/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHMessagingAvatarDatasource <NSObject>

@property (strong) NSMutableDictionary *mutableDictAvatars;

@end
