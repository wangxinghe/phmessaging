//
//  PHBasicConversationTableViewCellMessage.h
//  PHMessaging
//
//  Created by Ricol Wang on 24/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PHCommon/utils.h>

@interface PHBasicConversationTableViewCellMessage : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextView *tvMessage;
@property (strong) IBOutlet UIView *viewMyContent;

@end
