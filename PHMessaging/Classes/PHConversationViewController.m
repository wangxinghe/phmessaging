//
//  PHConversationViewController.m
//  PHMessaging
//
//  Created by Philology on 4/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHConversationViewController.h"
#import "PHMessagingManager.h"
#import "XMPPUser.h"
#import <PHCommon/utils.h>
#import "JSMessage.h"
#import "JSMessageSoundEffect.h"
#import "JSBubbleView.h"
#import <PHCommon/PHTextViewWithFrameAndPlaceholder.h>
#import "PHControlPanelViewController.h"
#import "PHConversationTableViewCellInMessage.h"
#import "PHConversationTableViewCellOutMessage.h"
#import "PHMessagingSplitViewController.h"
#import "UIColor+JSMessagesView.h"
#import "PHMessagingManager.h"

#define ANIMATE_TIME 0.2
#define HEIGHT_KEYBOARD 350

@interface PHConversationViewController () <UITextViewDelegate, PHControlPanelVCDelegate>

{
    BOOL bAlreadyRun;
    CGRect rectOriginalContent;
}

@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet PHTextViewWithFrameAndPlaceholder *tvInput;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (strong) UIPopoverController *popOver;

- (IBAction)btnClearOnTapped:(id)sender;
- (IBAction)btnMessageOnTapped:(id)sender;
- (IBAction)btnSendOnTapped:(id)sender;

@end

@implementation PHConversationViewController

@synthesize targetUser = _targetUser;

#pragma mark - View Life Cycle

- (instancetype)init
{
    self = [super initWithNibName:@"PHConversationViewController" bundle:nil];
    if (self)
    {

    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!bAlreadyRun)
    {
        if ([utils isIOS7])
            self.edgesForExtendedLayout = UIRectEdgeNone;
        
//        self.title = PHMessagingString(@"Conversation");
        self.lblTitle.text = PHMessagingString(@"Conversation");
        [self.btnSend setTitle:PHMessagingString(@"Send") forState:UIControlStateNormal];
        [self.btnMessage setTitle:PHMessagingString(@"Message") forState:UIControlStateNormal];
        [self.btnClear setTitle:PHMessagingString(@"Clear") forState:UIControlStateNormal];
        bAlreadyRun = YES;
        
#ifndef _DEBUG
        self.lblTitle.backgroundColor = [UIColor clearColor];
        self.viewTop.backgroundColor = [UIColor clearColor];
        self.viewBottom.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor whiteColor];
#endif
        
        self.tvInput.strPlaceholder = @"Please type message here!";
        
        self.targetUser = nil;
        
        if ([utils isIPhone])
        {
            self.viewTop.hidden = YES;
            self.viewContent.frame = self.view.bounds;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    rectOriginalContent = self.viewContent.frame;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self textViewDidEndEditing:self.tvInput];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    rectOriginalContent = self.viewContent.frame;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tmpStrTableViewCellInMessageIdentifier = @"PHConversationTableViewCellInMessage_Identifier";
    static NSString *tmpStrTableViewCellOutMesssageIdentifier = @"PHConversationTableViewCellOutMessage_Identifier";
    
    NSString *tmpStrName = @"";
    NSString *tmpStrDate = @"";
    NSString *tmpStrContent = @"";
    UIImage *tmpImageProfile = nil;
    BOOL bIsOutgoing = NO;
    
    XMPPMessageArchiving_Message_CoreDataObject *tmpMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    NSLog(@"%@ - %@: (%d, %d) tmpMessage.From: %@, tmpMessage.To: %@ (Outgoing: %@)", THIS_FILE, THIS_METHOD, indexPath.section, indexPath.row, tmpMessage.message.from.bare, tmpMessage.message.to.bare, tmpMessage.isOutgoing ? @"yes" : @"no");
    if ([tmpMessage isKindOfClass:[XMPPMessageArchiving_Message_CoreDataObject class]])
    {
        tmpStrContent = tmpMessage.body;
        tmpStrDate = [self.dfMessageDate stringFromDate:tmpMessage.timestamp];
        tmpImageProfile = [self avatarImageForRowAtIndexPath:indexPath];
        
        bIsOutgoing = tmpMessage.isOutgoing;
        if (bIsOutgoing)
        {
            tmpStrName = @"Me";
        }else
        {
            NSString *tmpStrFromUser = [[PHMessagingManager sharedPHMessagingManager] getUserNameForUserId:tmpMessage.message.from];
            tmpStrFromUser = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:tmpStrFromUser];
            tmpStrName = tmpStrFromUser;
        }
    }
    
    PHBasicConversationTableViewCellMessage *tmpCell = [tableView dequeueReusableCellWithIdentifier:bIsOutgoing ? tmpStrTableViewCellOutMesssageIdentifier : tmpStrTableViewCellInMessageIdentifier];
    
    if (!tmpCell)
    {
        if (bIsOutgoing)
        {
            static int outgoing = 0;
            outgoing++;
            NSLog(@"outgoing: %d", outgoing);
            tmpCell = [[[NSBundle mainBundle] loadNibNamed:@"PHConversationTableViewCellOutMessage" owner:self options:nil] objectAtIndex:0];
        }else
        {
            static int incoming = 0;
            incoming++;
            NSLog(@"incoming: %d", incoming);
            tmpCell = [[[NSBundle mainBundle] loadNibNamed:@"PHConversationTableViewCellInMessage" owner:self options:nil] objectAtIndex:0];
        }
    }
    
    tmpCell.lblName.text = tmpStrName;
    tmpCell.lblDate.text = tmpStrDate;
    tmpCell.tvMessage.text = tmpStrContent;
    
//    tmpCell.tvMessage.attributedText = [[NSAttributedString alloc] initWithString:tmpStrContent attributes:
//    @{
//      NSFontAttributeName : tmpCell.tvMessage.font
//    }];
    
    tmpCell.imageViewProfile.image = tmpImageProfile;
    
    UIImageView *tmpImageViewBubble = [self bubbleImageViewWithType:[self messageTypeForRowAtIndexPath:indexPath] forRowAtIndexPath:indexPath];
    tmpCell.imageViewBubble.image = [tmpImageViewBubble.image copy];
    
//    [self updateCell:&tmpCell];
    
    return tmpCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self updateCell:&cell];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [self updateCell:&cell];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.btnSend.enabled = textView.text.length > 0;
    self.btnClear.enabled = textView.text.length > 0;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    CGRect tmpRect = self.viewContent.frame;
    tmpRect.size.height -= HEIGHT_KEYBOARD;
    [UIView animateWithDuration:ANIMATE_TIME animations:^(){
        self.viewContent.frame = tmpRect;
    } completion:^(BOOL finished){
        [self scrollToBottom:YES];
    }];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self.tvInput resignFirstResponder];
    
    [UIView animateWithDuration:ANIMATE_TIME animations:^(){
        self.viewContent.frame = rectOriginalContent;
    } completion:^(BOOL finished){
        [self scrollToBottom:YES];
    }];
}

#pragma mark - Override Methods

- (void)setTargetUser:(NSManagedObject<XMPPUser>*)targetUser
{
    if (_targetUser != targetUser)
    {
        _targetUser = targetUser;
        // Update the user interface for the detail item.
        
        if (_targetUser)
        {
            self.title = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:self.targetUser.nickname];
            self.lblTitle.text = self.title;
        }
        
        self.fetchedResultsController = nil;
        
        [self.tableViewMain reloadData];
        [self scrollToBottom:YES];
    }
    
    self.viewTop.hidden = targetUser == nil;
    self.viewContent.hidden = targetUser == nil;
    self.viewBottom.hidden = targetUser == nil;
}

#pragma mark - PHControlPanelVCDelegate

- (void)PHControlPanelVCMessageSelected:(PHControlPanelViewController *)vc andMessage:(NSString *)Message
{
    self.tvInput.text = [NSString stringWithFormat:@"%@%@", self.tvInput.text, Message];
    self.btnSend.enabled = self.tvInput.text.length > 0;
    self.btnClear.enabled = self.tvInput.text.length > 0;
    
    if ([utils isIPhone])
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        [self.popOver dismissPopoverAnimated:NO];
        self.popOver = nil;
    }
}

#pragma mark - Private Methods

- (void)updateCell:(PHBasicConversationTableViewCellMessage **)cellInMessage
{
    float delta = 10;
    if ([*cellInMessage isKindOfClass:[PHConversationTableViewCellOutMessage class]])
    {
        PHConversationTableViewCellOutMessage *tmpCell = (PHConversationTableViewCellOutMessage *)*cellInMessage;
        
        NSString *tmpStr = tmpCell.tvMessage.text;
        
        CGSize tmpSize = tmpCell.viewContainer.frame.size;
        
        UIFont *tmpFont = tmpCell.tvMessage.font;
        
        CGSize stringSize;
        stringSize = [tmpStr boundingRectWithSize:tmpSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : tmpFont} context:nil].size;
        
        CGRect tmpRect;
        
        tmpRect.size.width = MIN(stringSize.width + delta * 2, tmpCell.lblDate.frame.size.width);
        tmpRect.size.height = tmpSize.height;
        
        tmpRect.origin = CGPointMake(tmpCell.imageViewProfile.frame.origin.x - tmpRect.size.width - delta, tmpCell.lblDate.frame.origin.y + tmpCell.lblDate.frame.size.height);
        
        tmpCell.tvMessage.frame = tmpRect;
        
        tmpRect.origin.x -= delta / 4;
        tmpRect.size.width += delta / 2;
        tmpCell.imageViewBubble.frame = tmpRect;
    }else if ([*cellInMessage isKindOfClass:[PHConversationTableViewCellInMessage class]])
    {
        PHConversationTableViewCellInMessage *tmpCell = (PHConversationTableViewCellInMessage *)*cellInMessage;
        
        NSString *tmpStr = tmpCell.tvMessage.text;
        
        CGSize tmpSize = tmpCell.viewContainer.frame.size;
        
        UIFont *tmpFont = tmpCell.tvMessage.font;
        
        CGSize stringSize;
        stringSize = [tmpStr boundingRectWithSize:tmpSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : tmpFont} context:nil].size;
        
        CGRect tmpRect;
        
        tmpRect.size.width = MIN(stringSize.width + delta * 2, tmpCell.lblDate.frame.size.width);
        tmpRect.size.height = tmpSize.height;
        
        tmpRect.origin = CGPointMake(tmpCell.lblDate.frame.origin.x, tmpCell.lblDate.frame.origin.y + tmpCell.lblDate.frame.size.height);
        
        tmpCell.tvMessage.frame = tmpRect;
        
        tmpRect.origin.x -= delta;
        tmpRect.size.width += delta / 2;
        tmpCell.imageViewBubble.frame = tmpRect;
    }
}

- (void)printRect:(CGRect)rect
{
    NSLog(@"rect: %f, %f, %f, %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}


#pragma mark - IBAction Methods

- (IBAction)btnClearOnTapped:(id)sender
{
    self.tvInput.text = @"";
    self.btnSend.enabled = NO;
    self.btnClear.enabled = NO;
}

- (IBAction)btnMessageOnTapped:(id)sender
{
    [self.popOver dismissPopoverAnimated:NO];
    self.popOver = nil;
    
    PHControlPanelViewController *tmpVC = [[PHControlPanelViewController alloc] init];
    tmpVC.delegate = self;
    
    if ([utils isIPhone])
    {
        [self.navigationController pushViewController:tmpVC animated:YES];
    }else
    {
        self.popOver = [[UIPopoverController alloc] initWithContentViewController:tmpVC];
        self.popOver.popoverContentSize = tmpVC.view.frame.size;
        [self.popOver presentPopoverFromRect:self.btnMessage.frame inView:self.btnMessage.superview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}

- (IBAction)btnSendOnTapped:(id)sender
{
    if ([self.tvInput.text isEqualToString:@""]) return;
    
    XMPPMessage *message = [[XMPPMessage alloc] initWithType:@"chat" to:[self.targetUser jid]];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:self.tvInput.text];
    [message addChild:body];

    [[[PHMessagingManager sharedPHMessagingManager] xmppStream] sendElement:message];

    [JSMessageSoundEffect playMessageSentSound];
    
    self.tvInput.text = @"";
    self.btnSend.enabled = NO;
    self.btnClear.enabled = NO;
    [self.tvInput resignFirstResponder];
}

@end
