//
//  PHContactsVCDelegate.h
//  PHMessaging
//
//  Created by Ricol Wang on 4/12/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PHContactsViewController;

@protocol PHContactsVCDelegate <NSObject>

- (void)PHContactsVCQuit:(PHContactsViewController *)vc;

@end
