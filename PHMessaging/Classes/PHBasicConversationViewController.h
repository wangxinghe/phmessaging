//
//  PHBasicConversationViewController.h
//  PHMessaging
//
//  Created by Ricol Wang on 1/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicViewController.h"
#import <XMPPFramework/XMPPUser.h>
#import <JSMessagesViewController/JSBubbleView.h>
#import <CoreData/CoreData.h>
#import "PHMessagingAvatarDatasource.h"

@class PHMessagingSplitViewController;

@interface PHBasicConversationViewController : PHBasicViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject<XMPPUser> *targetUser;
@property (strong) NSDateFormatter *dfMessageDate;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;

- (void)initialize;
- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath;
- (UIImageView *)bubbleImageViewWithType:(JSBubbleMessageType)type
                       forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)scrollToTop:(BOOL)animate;
- (void)scrollToBottom:(BOOL)animate;
- (UIImage *)avatarImageForRowAtIndexPath:(NSIndexPath *)indexPath;
- (float)getHeightForString:(NSString *)string;

@end
