//
//  PHMessagingSplitViewController.m
//  PHMessaging
//
//  Created by Philology on 15/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHMessagingSplitViewController.h"
#import "PHMessagingManager.h"
#import <PHCommon/utils.h>
#import "PHContactListViewController.h"

@interface PHMessagingSplitViewController () <XMPPStreamDelegate>

{
    BOOL bAlreadyRun;
}

@property (weak) id <PHMessagingMainVCDelegate> delegateForMainVC;

@property (strong, nonatomic) PHContactsViewController *contactsVC;
@property (strong, nonatomic) PHConversationViewController *conversationVC;

@property (strong, nonatomic) UINavigationController *navMaster;
@property (strong, nonatomic) UINavigationController *navDetail;

@property (strong) UIBarButtonItem *barBtnItemClose;
@property (strong) UIBarButtonItem *barBtnItemBroadcast;

@end

@implementation PHMessagingSplitViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.viewControllers = @[self.navMaster, self.navDetail];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([self respondsToSelector:@selector(setPreferredDisplayMode:)])
    {
        if ([utils isIPhone])
            [self setPreferredDisplayMode:UISplitViewControllerDisplayModeAutomatic];
        else
            [self setPreferredDisplayMode:UISplitViewControllerDisplayModeAllVisible];
    }
    
    self.title = PHMessagingString(@"Message");
    
    if ([utils isIOS7])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.barBtnItemBroadcast = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Team"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBroadcastOnTapped:)];
    self.navigationItem.rightBarButtonItem = self.barBtnItemBroadcast;
    
    self.barBtnItemClose = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCloseOnTapped:)];
    self.navigationItem.leftBarButtonItem = self.barBtnItemClose;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!bAlreadyRun)
    {
        bAlreadyRun = YES;
    }
    
    [[PHMessagingManager sharedPHMessagingManager] AddDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[PHMessagingManager sharedPHMessagingManager] RemoveDelegate:self];
}

#pragma mark - Override Methods

- (PHContactsViewController *)contactsVC
{
    if (!_contactsVC)
    {
        _contactsVC = [[PHContactsViewController alloc] initWithConversation:self.conversationVC];
    }
    
    return _contactsVC;
}

- (PHConversationViewController *)conversationVC
{
    if (!_conversationVC)
    {
        _conversationVC = [[PHConversationViewController alloc] init];
    }
    
    return _conversationVC;
}

- (UINavigationController *)navMaster
{
    if (!_navMaster)
    {
        _navMaster = [[UINavigationController alloc] initWithRootViewController:self.contactsVC];
        _navMaster.navigationBarHidden = YES;
    }
    
    return _navMaster;
}

- (UINavigationController *)navDetail
{
    if (!_navDetail)
    {
        _navDetail = [[UINavigationController alloc] initWithRootViewController:self.conversationVC];
        _navDetail.navigationBarHidden = YES;
    }
    
    return _navDetail;
}

#pragma mark - XMPPStreamDelegate

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    NSString *tmpStrUserName = [[PHMessagingManager sharedPHMessagingManager] getUserNameForUserId:message.from];
    tmpStrUserName = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:tmpStrUserName];
    
    if (tmpStrUserName)
    {
        NSString *tmpStrMessageBody = message.body;
        self.title = [NSString stringWithFormat:@"%@ %@ : %@", PHMessagingString(@"MessageFrom"), tmpStrUserName, tmpStrMessageBody.length < 50 ? [NSString stringWithFormat:@"\"%@\"", tmpStrMessageBody] : @"..."];
    }
    else
        self.title = @"";
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    NSString *tmpStrUserName = [[PHMessagingManager sharedPHMessagingManager] getUserNameForUserId:presence.from];
    tmpStrUserName = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:tmpStrUserName];
    if (tmpStrUserName)
    {
        NSString *tmpStrType = presence.type;
        tmpStrType = [tmpStrType uppercaseString];
        if ([tmpStrType isEqualToString:@"AVAILABLE"])
            tmpStrType = PHMessagingString(@"Online");
        else if ([tmpStrType isEqualToString:@"UNAVAILABLE"])
            tmpStrType = PHMessagingString(@"Offline");
        
        self.title = [NSString stringWithFormat:@"%@ is %@!", tmpStrUserName, tmpStrType];
    }
    else
        self.title = @"";
}

#pragma mark - Public Methods

+ (UIViewController *)launchChattingWithDelegate:(id <PHMessagingMainVCDelegate>)delegate
{
    UIViewController *tmpVC = nil;
    
    if ([utils isIPhone])
    {
        PHContactsViewController *tmpContactVC = [[PHContactsViewController alloc] init];
        UINavigationController *tmpNav = [[UINavigationController alloc] initWithRootViewController:tmpContactVC];
        tmpVC = tmpNav;
    }else
    {
        UIStoryboard *tmpStoryboard = [UIStoryboard storyboardWithName:@"PHMessaging_iPad" bundle:nil];
        if (tmpStoryboard)
        {
            tmpVC = [tmpStoryboard instantiateInitialViewController];
            if (tmpVC)
            {
                if ([tmpVC isKindOfClass:[UINavigationController class]])
                {
                    UIViewController *tmpMessageVC = ((UINavigationController *)tmpVC).topViewController;
                    
                    if ([tmpMessageVC isKindOfClass:[PHMessagingSplitViewController class]])
                    {
                        ((PHMessagingSplitViewController *)tmpMessageVC).delegateForMainVC = delegate;
                    }
                }
            }
        }
    }
    
    return tmpVC;
}

#pragma mark - Private Methods

- (void)btnBroadcastOnTapped:(id)sender
{
    PHContactListViewController *tmpVC = [[PHContactListViewController alloc] initWithConversation:self.conversationVC];

    UINavigationController *tmpNav = [[UINavigationController alloc] initWithRootViewController:tmpVC];
    tmpNav.modalPresentationStyle = UIModalPresentationFormSheet;
    if ([utils isIOS8])
        tmpNav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    else
        tmpNav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:tmpNav animated:YES completion:nil];
}

- (void)btnCloseOnTapped:(id)sender
{
    [self.delegateForMainVC PHMessagingMainVCQuit:self];
}

@end
