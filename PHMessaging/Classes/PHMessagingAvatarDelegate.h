//
//  PHMessagingAvatarDelegate.h
//  PHMessaging
//
//  Created by Ricol Wang on 21/03/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHMessagingAvatarDelegate <NSObject>

@optional
- (UIImage *)PHMessagingOnLineAvatarForUserId:(NSString *)userId;
- (UIImage *)PHMessagingOffLineAvatarForUserId:(NSString *)userId;

@end
