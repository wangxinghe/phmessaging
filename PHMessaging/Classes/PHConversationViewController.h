//
//  PHConversationViewController.h
//  PHMessaging
//
//  Created by Philology on 4/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicConversationViewController.h"

@interface PHConversationViewController : PHBasicConversationViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
