//
//  PHControlPanelViewController.h
//  PHMessaging
//
//  Created by Ricol Wang on 22/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHControlPanelVCDelegate.h"

@interface PHControlPanelViewController : UIViewController

@property (weak) id <PHControlPanelVCDelegate> delegate;

@end
