//
//  PHMessagingContactNameDataSource.h
//  PHMessaging
//
//  Created by Ricol Wang on 12/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHMessagingContactNameDataSource <NSObject>

- (NSString *)PHMessagingContactNameReplace:(NSString *)nameOriginal;

@end
