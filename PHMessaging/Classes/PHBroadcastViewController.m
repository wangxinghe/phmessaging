//
//  PHBroadcastViewController.m
//  PHMessaging
//
//  Created by Ricol Wang on 20/03/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBroadcastViewController.h"
#import "PHMessagingManager.h"
#import <JSMessagesViewController/JSMessagesViewController.h>
#import <PHCommon/utils.h>
#import <PHCommon/PHTextViewWithFrameAndPlaceholder.h>
#import "PHControlPanelViewController.h"

@interface PHBroadcastViewController () <UIAlertViewDelegate, PHControlPanelVCDelegate>

{
    BOOL bAlreadyRun;
}

@property (weak, nonatomic) IBOutlet PHTextViewWithFrameAndPlaceholder *tvText;

@property (strong) UIBarButtonItem *barBtnItemSend;
@property (strong) NSArray *arrayUsers;

- (IBAction)btnMessagesOnTapped:(id)sender;

@end

@implementation PHBroadcastViewController

- (instancetype)initWithUsers:(NSArray *)users
{
    self = [super initWithNibName:@"PHBroadcastViewController" bundle:nil];
    if (self)
    {
        self.arrayUsers = users;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([utils isIOS7])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.barBtnItemSend = [[UIBarButtonItem alloc] initWithTitle:PHMessagingString(@"Send")style:UIBarButtonItemStylePlain target:self action:@selector(btnBroadcastOnTapped:)];
    self.navigationItem.rightBarButtonItem = self.barBtnItemSend;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!bAlreadyRun)
    {
        self.title = PHMessagingString(@"BroadcastMessage");
        self.tvText.strPlaceholder = [NSString stringWithFormat:@"%@!", PHMessagingString(@"TypeMessageHere")];
        self.tvText.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.tvText.backgroundColor = [UIColor whiteColor];
        
        bAlreadyRun = YES;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if (self.arrayUsers)
        {
            NSString *tmpStrText = self.tvText.text;
            tmpStrText = [utils trimBoth:tmpStrText];
            
            if (![tmpStrText isEqualToString:@""])
            {
                for (XMPPJID *tmpID in self.arrayUsers)
                {
                    NSLog(@"%@ - %@: Send the message to %@", THIS_FILE, THIS_METHOD, tmpID.bare);
                    
                    XMPPMessage *message = [[XMPPMessage alloc] initWithType:@"chat" to:tmpID];
                    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:tmpStrText];
                    [message addChild:body];
                    
                    [[[PHMessagingManager sharedPHMessagingManager] xmppStream] sendElement:message];
                }
                
                [JSMessageSoundEffect playMessageSentSound];
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }
}

#pragma mark - PHControlPanelVCDelegate

- (void)PHControlPanelVCMessageSelected:(PHControlPanelViewController *)vc andMessage:(NSString *)Message
{
    self.tvText.text = [NSString stringWithFormat:@"%@%@", self.tvText.text, Message];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private Methods

- (void)btnBroadcastOnTapped:(id)sender
{
    if (self.arrayUsers && self.arrayUsers.count > 0)
    {
        NSString *tmpStrText = self.tvText.text;
        tmpStrText = [utils trimBoth:tmpStrText];
        
        if (![tmpStrText isEqualToString:@""])
        {
            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:PHMessagingString(@"Confirm") message:PHMessagingString(@"ConfirmSendMessage") delegate:self cancelButtonTitle:PHMessagingString(@"Yes") otherButtonTitles:PHMessagingString(@"No"), nil];
            [tmpAlertView show];
        }else
        {
            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:PHMessagingString(@"Warning") message:PHMessagingString(@"PleaseWriteMessage") delegate:nil cancelButtonTitle:PHMessagingString(@"Ok") otherButtonTitles:nil];
            [tmpAlertView show];
            [self.tvText becomeFirstResponder];
        }
    }else
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:PHMessagingString(@"Error") message:PHMessagingString(@"PleaseSelectContacts") delegate:nil cancelButtonTitle:PHMessagingString(@"Ok") otherButtonTitles:nil];
        [tmpAlertView show];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction Methods

- (IBAction)btnMessagesOnTapped:(id)sender
{
    PHControlPanelViewController *tmpVC = [[PHControlPanelViewController alloc] init];
    tmpVC.delegate = self;
    [self.navigationController pushViewController:tmpVC animated:YES];
}

@end
