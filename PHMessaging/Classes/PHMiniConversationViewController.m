//
//  PHMiniConversationViewController.m
//  PHMessaging
//
//  Created by Ricol Wang on 1/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHMiniConversationViewController.h"
#import "PHTextViewWithFrameAndPlaceholder.h"
#import <CoreData/CoreData.h>
#import "JSMessage.h"
#import "JSMessageSoundEffect.h"
#import "JSBubbleView.h"
#import "PHMessagingManager.h"
#import "PHMiniConversationTableViewCell.h"
#import "PHControlPanelViewController.h"
#import <PHCommon/PHKeyBoardManager.h>

@interface PHMiniConversationViewController () <PHControlPanelVCDelegate>

@property (weak, nonatomic) IBOutlet PHTextViewWithFrameAndPlaceholder *tvInput;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;

- (IBAction)btnMessageOnTapped:(id)sender;
@property (strong) NSDateFormatter *dfMessageDate;

@end

@implementation PHMiniConversationViewController

@synthesize targetUser = _targetUser;

#pragma mark - View Life Cycle

- (instancetype)init
{
    self = [super initWithNibName:@"PHMiniConversationViewController" bundle:nil];
    if (self)
    {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initialize];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableViewMain reloadData];
    
    if ([utils isIPhone])
        [[PHKeyBoardManager sharedKeyBoardManager] Enable:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self releaseKeyboard];
    
    if ([utils isIPhone])
        [[PHKeyBoardManager sharedKeyBoardManager] Enable:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tmpStrTableViewCell_Identifier = @"PHMiniConversationTableViewCell_Identifier";
    
    PHMiniConversationTableViewCell *tmpCell = [tableView dequeueReusableCellWithIdentifier:tmpStrTableViewCell_Identifier];
    
    if (!tmpCell)
    {
        tmpCell = [[[NSBundle mainBundle] loadNibNamed:@"PHMiniConversationTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    BOOL bIsOutgoing = NO;
    NSString *tmpStrName = @"";
    NSString *tmpStrContent = @"";
    NSString *tmpStrDate = @"";
    
    XMPPMessageArchiving_Message_CoreDataObject *tmpMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //    NSLog(@"%@ - %@: (%d, %d) tmpMessage.From: %@, tmpMessage.To: %@ (Outgoing: %@)", THIS_FILE, THIS_METHOD, indexPath.section, indexPath.row, tmpMessage.message.from.bare, tmpMessage.message.to.bare, tmpMessage.isOutgoing ? @"yes" : @"no");
    if ([tmpMessage isKindOfClass:[XMPPMessageArchiving_Message_CoreDataObject class]])
    {
        tmpStrContent = tmpMessage.body;
        tmpStrDate = [self.dfMessageDate stringFromDate:tmpMessage.timestamp];
        
        bIsOutgoing = tmpMessage.isOutgoing;
        if (bIsOutgoing)
        {
            tmpStrName = @"Me";
        }else
        {
            NSString *tmpStrFromUser = [[PHMessagingManager sharedPHMessagingManager] getUserNameForUserId:tmpMessage.message.from];
            tmpStrFromUser = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:tmpStrFromUser];
            tmpStrName = tmpStrFromUser;
        }
    }
    
    tmpCell.lblName.text = tmpStrName;
    tmpCell.lblDate.text = tmpStrDate;
    tmpCell.tvMessage.text = tmpStrContent;
    
    return tmpCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float heightBase = 52 + 10;
    float widthBase = 180;
    float heightRow = heightBase;
    
    XMPPMessageArchiving_Message_CoreDataObject *tmpMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([tmpMessage isKindOfClass:[XMPPMessageArchiving_Message_CoreDataObject class]])
    {
        NSAttributedString *tmpStrMessage = [[NSAttributedString alloc] initWithString:tmpMessage.body attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15]}];
        
        float heightText = 0;
        float widthText = self.tableViewMain.frame.size.width - widthBase;
        heightText = [tmpStrMessage boundingRectWithSize:CGSizeMake(widthText, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
        
        if (heightText > heightBase) heightRow = heightText;
    }
    
    return heightRow;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - PHControlPanelVCDelegate

- (void)PHControlPanelVCMessageSelected:(PHControlPanelViewController *)vc andMessage:(NSString *)Message
{
    self.tvInput.text = [NSString stringWithFormat:@"%@%@", self.tvInput.text, Message];
    
    if (self.navigationController)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Override Methods

- (void)setTargetUser:(NSManagedObject<XMPPUser>*)targetUser
{
    if (_targetUser != targetUser)
    {
        _targetUser = targetUser;
        // Update the user interface for the detail item.
        
        if (_targetUser)
        {
            self.title = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:self.targetUser.nickname];
        }
        
        self.fetchedResultsController = nil;
        
        [self.tableViewMain reloadData];
        [self scrollToBottom:YES];
    }
    
    self.view.alpha = targetUser == nil ? 0.2 : 1;
    self.view.userInteractionEnabled = !(targetUser == nil);
}

#pragma mark - IBAction Methods

- (IBAction)btnSendOnTapped:(id)sender
{
    if ([self.tvInput.text isEqualToString:@""]) return;
    
    XMPPMessage *message = [[XMPPMessage alloc] initWithType:@"chat" to:[self.targetUser jid]];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:self.tvInput.text];
    [message addChild:body];
    
    [[[PHMessagingManager sharedPHMessagingManager] xmppStream] sendElement:message];
    
    [JSMessageSoundEffect playMessageSentSound];
    
    self.tvInput.text = @"";

    [self releaseKeyboard];
}

#pragma mark - Private Methods

- (void)releaseKeyboard
{
    [self.tvInput resignFirstResponder];
}

- (void)initialize
{
    self.dfMessageDate = [[NSDateFormatter alloc] init];
    self.dfMessageDate.dateFormat = @"HH:mm:ss dd/MM/yyyy";
    
    self.targetUser = nil;
    
    self.tvInput.strPlaceholder = @"Input message!";
    [self.tvInput resignFirstResponder];
}

- (IBAction)btnMessageOnTapped:(id)sender
{
    [self resignFirstResponder];
    
    PHControlPanelViewController *tmpVC = [[PHControlPanelViewController alloc] init];
    tmpVC.delegate = self;
    
    if (self.navigationController)
    {
        [self.navigationController pushViewController:tmpVC animated:YES];
    }else
    {
        tmpVC.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:tmpVC animated:YES completion:nil];
    }
}

@end
