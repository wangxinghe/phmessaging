//
//  PHConversationTableViewCellOutMessage.m
//  PHMessaging
//
//  Created by Ricol Wang on 24/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHConversationTableViewCellOutMessage.h"

@implementation PHConversationTableViewCellOutMessage

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
