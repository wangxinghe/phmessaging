//
//  PHMessagingMainVCDelegate.h
//  PHMessaging
//
//  Created by Ricol Wang on 9/04/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PHMessagingSplitViewController;

@protocol PHMessagingMainVCDelegate <NSObject>

- (void)PHMessagingMainVCQuit:(PHMessagingSplitViewController *)vc;

@end
