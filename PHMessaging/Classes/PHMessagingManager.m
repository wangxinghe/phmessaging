//
//  PHMessagingManager.m
//  PHMessaging
//
//  Created by Philology on 6/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHMessagingManager.h"
#import <PHCommon/utils.h>

// Log levels: off, error, warn, info, verbose
#if DEBUG
 int PHMessagingLogLevel = LOG_LEVEL_WARN;
#else
 int PHMessagingLogLevel = LOG_LEVEL_ERROR;
#endif

NSString *const kXMPPmyJID = @"kXMPPmyJID";
NSString *const kXMPPmyPassword = @"kXMPPmyPassword";

@interface PHMessagingManager () <XMPPStreamDelegate>

{
	BOOL bAllowSelfSignedCertificates;
	BOOL bAllowSSLHostNameMismatch;
	BOOL bIsXmppConnected;
}

@property (strong) NSMutableDictionary *mutableDictUserNameMapping;
@property (strong) NSMutableArray *mutableArrayDelegates;

@property (copy) NSString *userJID;
@property (copy) NSString *userPassword;

- (void)setupStream;
- (void)teardownStream;
- (void)goOnline;
- (void)goOffline;

@end


@implementation PHMessagingManager

- (id)init
{
    self = [super init];
    if (self)
    {
        self.mutableArrayDelegates = [NSMutableArray new];
        self.mutableDictUserNameMapping = [NSMutableDictionary new];
        self.delegateForAvatar = self;
        [self observeNotifications];
    }
    return self;
}

- (void)dealloc
{
	[self teardownStream];
}

#pragma mark - XMPPStreamDelegate

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStream:socketDidConnect:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStream:sender socketDidConnect:socket];
    }
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	if (bAllowSelfSignedCertificates)
	{
		[settings setObject:[NSNumber numberWithBool:YES] forKey:(NSString *)kCFStreamSSLAllowsAnyRoot];
	}
	
	if (bAllowSSLHostNameMismatch)
	{
		[settings setObject:[NSNull null] forKey:(NSString *)kCFStreamSSLPeerName];
	}
	else
	{
		// Google does things incorrectly (does not conform to RFC).
		// Because so many people ask questions about this (assume xmpp framework is broken),
		// I've explicitly added code that shows how other xmpp clients "do the right thing"
		// when connecting to a google server (gmail, or google apps for domains).
		
		NSString *expectedCertName = nil;
		
		NSString *serverDomain = self.xmppStream.hostName;
		NSString *virtualDomain = [self.xmppStream.myJID domain];
		
		if ([serverDomain isEqualToString:@"talk.google.com"])
		{
			if ([virtualDomain isEqualToString:@"gmail.com"])
			{
				expectedCertName = virtualDomain;
			}
			else
			{
				expectedCertName = serverDomain;
			}
		}
		else if (serverDomain == nil)
		{
			expectedCertName = virtualDomain;
		}
		else
		{
			expectedCertName = serverDomain;
		}
		
		if (expectedCertName)
		{
			[settings setObject:expectedCertName forKey:(NSString *)kCFStreamSSLPeerName];
		}
	}
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStream:willSecureWithSettings:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStream:sender willSecureWithSettings:settings];
    }
}

- (void)xmppStreamDidSecure:(XMPPStream *)sender
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStreamDidSecure:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStreamDidSecure:sender];
    }
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	bIsXmppConnected = YES;
	
	NSError *error = nil;
    
    id <XMPPSASLAuthentication> authentication = [[XMPPPlainAuthentication alloc] initWithStream:self.xmppStream password:self.userPassword];
	
    if (![[self xmppStream] authenticate:authentication error:&error])
	{
		NSLog(@"Error authenticating: %@", error);
	}
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStreamDidConnect:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStreamDidConnect:sender];
    }
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	[self goOnline];
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStreamDidAuthenticate:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStreamDidAuthenticate:sender];
    }
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStream:didNotAuthenticate:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStream:sender didNotAuthenticate:error];
    }
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	return NO;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
	// A simple example of inbound message handling.
    
    NSLog(@"message: %@", message);
	if ([message isChatMessageWithBody])
	{
		XMPPUserCoreDataStorageObject *user = [self.xmppRosterStorage userForJID:[message from]
                                                                      xmppStream:self.xmppStream
                                                            managedObjectContext:[self managedObjectContext_roster]];
		
		NSString *body = [[message elementForName:@"body"] stringValue];
		NSString *displayName = [user displayName];
        
        if (displayName && body)
        {
            for (NSObject *tmpObject in self.mutableArrayDelegates)
            {
                if ([tmpObject respondsToSelector:@selector(xmppStream:didReceiveMessage:)])
                    [(id <XMPPStreamDelegate>)tmpObject xmppStream:sender didReceiveMessage:message];
            }
        }
	}
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
	NSLog(@"%@: %@ - %@", THIS_FILE, THIS_METHOD, [presence fromStr]);
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStream:didReceivePresence:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStream:sender didReceivePresence:presence];
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStream:didReceiveError:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStream:sender didReceiveError:error];
    }
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
	NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	if (!bIsXmppConnected)
	{
		NSLog(@"Unable to connect to server. Check xmppStream.hostName");
	}
    
    for (NSObject *tmpObject in self.mutableArrayDelegates)
    {
        if ([tmpObject respondsToSelector:@selector(xmppStreamDidDisconnect:withError:)])
            [(id <XMPPStreamDelegate>)tmpObject xmppStreamDidDisconnect:sender withError:error];
    }
}

#pragma mark - PHMessagingAvatarDelegate

- (UIImage *)PHMessagingAvatarForUserId:(NSString *)userId
{
    NSString *tmpStrFileName = [NSString stringWithFormat:@"%@/%@.png", DOCUMENTS_DIRECTORY, userId];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL bDirectory;
    BOOL bExists = NO;
    if ([fm fileExistsAtPath:tmpStrFileName isDirectory:&bDirectory])
    {
        bExists = YES;
    }else
    {
        tmpStrFileName = [NSString stringWithFormat:@"%@/%@.jpg", DOCUMENTS_DIRECTORY, userId];
        if ([fm fileExistsAtPath:tmpStrFileName isDirectory:&bDirectory])
            bExists = YES;
    }
    
    if (bExists)
    {
        NSData *tmpData = [[NSData alloc] initWithContentsOfFile:tmpStrFileName];
        UIImage *tmpImage = [UIImage imageWithData:tmpData];
        return tmpImage;
    }
    return nil;
}

#pragma mark - Class Methods

+ (PHMessagingManager *)sharedPHMessagingManager
{
    static dispatch_once_t onceQueue;
    static PHMessagingManager *pHMessagingManager = nil;
    
    dispatch_once(&onceQueue, ^{ pHMessagingManager = [[self alloc] init];
        [pHMessagingManager handleUIApplicationDidFinishLaunchingNotification];
    });
    return pHMessagingManager;
}

+ (id)setupSharedPHMessagingManager
{
    PHMessagingManager *manager = [PHMessagingManager sharedPHMessagingManager];
    [manager observeNotifications];
    return manager;
}

#pragma mark - Private Methods

- (void)observeNotifications
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUIApplicationDidFinishLaunchingNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUIApplicationDidEnterBackgroundNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUIApplicationWillEnterForegroundNotification) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)removeObserveNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleUIApplicationDidFinishLaunchingNotification
{
    [self setupStream];
    
    if (![self connect])
    {
        
    }
}

- (void)handleUIApplicationDidEnterBackgroundNotification
{
#if TARGET_IPHONE_SIMULATOR
	NSLog(@"The iPhone simulator does not process background network traffic. "
          @"Inbound traffic is queued until the keepAliveTimeout:handler: fires.");
#endif
    
    UIApplication *application = [UIApplication sharedApplication];
    
	if ([application respondsToSelector:@selector(setKeepAliveTimeout:handler:)])
	{
		[application setKeepAliveTimeout:600 handler:^{
			
			NSLog(@"KeepAliveHandler");
			
			// Do other keep alive stuff here.
		}];
	}
}

- (void)handleUIApplicationWillEnterForegroundNotification
{
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)setupStream
{
	NSAssert(self.xmppStream == nil, @"Method setupStream invoked multiple times");
	
	// Setup xmpp stream
	//
	// The XMPPStream is the base class for all activity.
	// Everything else plugs into the xmppStream, such as modules/extensions and delegates.
    
	self.xmppStream = [[XMPPStream alloc] init];
	
    #if !TARGET_IPHONE_SIMULATOR
	{
		// Want xmpp to run in the background?
		//
		// P.S. - The simulator doesn't support backgrounding yet.
		//        When you try to set the associated property on the simulator, it simply fails.
		//        And when you background an app on the simulator,
		//        it just queues network traffic til the app is foregrounded again.
		//        We are patiently waiting for a fix from Apple.
		//        If you do enableBackgroundingOnSocket on the simulator,
		//        you will simply see an error message from the xmpp stack when it fails to set the property.
		
		self.xmppStream.enableBackgroundingOnSocket = YES;
	}
    #endif
	
	self.xmppReconnect = [[XMPPReconnect alloc] init];
	
	self.xmppRosterStorage = [XMPPRosterCoreDataStorage sharedInstance];
	self.xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:self.xmppRosterStorage];
	self.xmppRoster.autoFetchRoster = YES;
	self.xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
	
	// Setup vCard support
	//
	// The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
	// The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
	self.xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
	self.xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:self.xmppvCardStorage];
	self.xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:self.xmppvCardTempModule];
    
    self.xmppMessageArchivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    self.xmppMessageArchiving = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:self.xmppMessageArchivingStorage];
	
	self.xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    self.xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:self.xmppCapabilitiesStorage];
    
    self.xmppCapabilities.autoFetchHashedCapabilities = YES;
    self.xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    
	// Activate xmpp modules
    
	[self.xmppReconnect         activate:self.xmppStream];
	[self.xmppRoster            activate:self.xmppStream];
	[self.xmppvCardTempModule   activate:self.xmppStream];
	[self.xmppvCardAvatarModule activate:self.xmppStream];
    [self.xmppMessageArchiving  activate:self.xmppStream];
	[self.xmppCapabilities      activate:self.xmppStream];
    
	// Add ourself as a delegate to anything we may be interested in
    
	[self.xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
	// If you don't supply a hostName, then it will be automatically resolved using the JID (below).
	// For example, if you supply a JID like 'user@quack.com/rsrc'
	// then the xmpp framework will follow the xmpp specification, and do a SRV lookup for quack.com.
	//
	// If you don't specify a hostPort, then the default (5222) will be used.
    if (self.hostname)
    {
        [self.xmppStream setHostName:self.hostname];
    }
    if (self.port)
    {
        [self.xmppStream setHostPort:[self.port unsignedIntValue]];
    }	
    
	// You may need to alter these settings depending on the server you're connecting to
	bAllowSelfSignedCertificates = YES;
	bAllowSSLHostNameMismatch    = YES;
}

- (void)teardownStream
{
	[self.xmppStream removeDelegate:self];
	
	[self.xmppReconnect         deactivate];
	[self.xmppRoster            deactivate];
	[self.xmppvCardTempModule   deactivate];
	[self.xmppvCardAvatarModule deactivate];
    [self.xmppMessageArchiving  deactivate];
	[self.xmppCapabilities      deactivate];
	
	[self.xmppStream disconnect];
}

// It's easy to create XML elments to send and to read received XML elements.
// You have the entire NSXMLElement and NSXMLNode API's.
//
// In addition to this, the NSXMLElement+XMPP category provides some very handy methods for working with XMPP.
//
// On the iPhone, Apple chose not to include the full NSXML suite.
// No problem - we use the KissXML library as a drop in replacement.
//
// For more information on working with XML elements, see the Wiki article:
// http://code.google.com/p/xmppframework/wiki/WorkingWithElements

- (void)goOnline
{
	XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit
	
	[[self xmppStream] sendElement:presence];
}

- (void)goOffline
{
	XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
	
	[[self xmppStream] sendElement:presence];
}

#pragma mark - Public Methods

- (void)AddDelegate:(id <XMPPStreamDelegate>) delegate
{
    if ([self.mutableArrayDelegates containsObject:delegate]) return;
    [self.mutableArrayDelegates addObject:delegate];
}

- (void)RemoveDelegate:(id <XMPPStreamDelegate>) delegate
{
    if ([self.mutableArrayDelegates containsObject:delegate]) [self.mutableArrayDelegates removeObject:delegate];
}

- (NSString *)getUserNameForUserId:(XMPPJID *)userId
{
    if (userId)
    {
        if (userId.bare)
        {
            if (!self.mutableDictUserNameMapping[userId.bare])
            {
                XMPPUserCoreDataStorageObject *user = [self.xmppRosterStorage userForJID:userId
                                                                              xmppStream:self.xmppStream
                                                                    managedObjectContext:[self managedObjectContext_roster]];
                if (user)
                {
                    NSString *displayName = [user displayName];
                    if (displayName)
                    {
                        self.mutableDictUserNameMapping[userId.bare] = displayName;
                        return [self.mutableDictUserNameMapping[userId.bare] copy];
                    }
                }
            }else
                return [self.mutableDictUserNameMapping[userId.bare] copy];
        }
    }
    
    return nil;
}

- (NSManagedObjectContext *)managedObjectContext_roster
{
	return [self.xmppRosterStorage mainThreadManagedObjectContext];
}

- (NSManagedObjectContext *)managedObjectContext_capabilities
{
	return [self.xmppCapabilitiesStorage mainThreadManagedObjectContext];
}

- (NSManagedObjectContext *)managedObjectContext_messages
{
    return [self.xmppMessageArchivingStorage mainThreadManagedObjectContext];
}

- (BOOL)connectWithUser:(NSString*)userJID password:(NSString*)password
{
    self.userJID = userJID;
    self.userPassword = password;
    return [self connect];
}

- (BOOL)connect
{
	if (![self.xmppStream isDisconnected])
		return YES;
    
	NSString *myJID = self.userJID;
	NSString *myPassword = self.userPassword;
    
	//myJID = @"62_CPM@www.aglwalkcover.com";
	//myPassword = @"cookie";
	
	if (myJID == nil || myPassword == nil)
		return NO;
    
	[self.xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
	self.userPassword = myPassword;
    
	NSError *error = nil;
    
	if (![self.xmppStream connectWithTimeout:10 error:&error])
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
		                                                    message:@"See console for error details."
		                                                   delegate:nil
		                                          cancelButtonTitle:@"Ok"
		                                          otherButtonTitles:nil];
		[alertView show];
        
		NSLog(@"Error connecting: %@", error);
        
		return NO;
	}
    
	return YES;
}

- (void)disconnect
{
	[self goOffline];
	[self.xmppStream disconnect];
}

- (UIImage *)getOnLineAvatarForUserId:(NSString *)userId
{
    if (userId)
    {
        if ([self.delegateForAvatar respondsToSelector:@selector(PHMessagingOnLineAvatarForUserId:)])
            return [self.delegateForAvatar PHMessagingOnLineAvatarForUserId:userId];
        else
            return nil;
    }else
        return nil;
}

- (UIImage *)getOffLineAvatarForUserId:(NSString *)userId
{
    if (userId)
    {
        if ([self.delegateForAvatar respondsToSelector:@selector(PHMessagingOffLineAvatarForUserId:)])
            return [self.delegateForAvatar PHMessagingOffLineAvatarForUserId:userId];
        else
            return nil;
    }else
        return nil;
}

- (NSString *)getUserNameForOldName:(NSString *)oldName
{
    if (self.dataSourceForContactName)
        return [self.dataSourceForContactName PHMessagingContactNameReplace:oldName];
    else
        return oldName;
}

- (NSArray *)getMessageList
{
    if (self.dataSourceForMessageList)
        return [self.dataSourceForMessageList PHMessageListGetListOfMessages];
    else
        return @[@{KEY_MESSAGE_LIST_TITLE : @"Hello!"},
                 @{KEY_MESSAGE_LIST_TITLE : @"Bye!"},
                 @{KEY_MESSAGE_LIST_TITLE : @"Ok!"},
                 @{KEY_MESSAGE_LIST_TITLE : @"Time to go!"},
                 @{KEY_MESSAGE_LIST_TITLE : @"Launch Time!"},
                 @{KEY_MESSAGE_LIST_TITLE : @"We are leaving!"},
                 @{KEY_MESSAGE_LIST_TITLE : @"Where are you?"},
                 @{KEY_MESSAGE_LIST_TITLE : @"See you!"}];
}

+ (void)broadcaseMessage:(NSString *)message
{
    NSArray *tmpArrayAllUsers = [NSArray new];
    
    for (XMPPJID *tmpID in tmpArrayAllUsers)
    {
        NSLog(@"%@ - %@: Broadcast the message to %@", THIS_FILE, THIS_METHOD, tmpID.bare);
        
        XMPPMessage *tmpMessage = [[XMPPMessage alloc] initWithType:@"chat" to:tmpID];
        NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:message];
        [tmpMessage addChild:body];
        
        [[[PHMessagingManager sharedPHMessagingManager] xmppStream] sendElement:tmpMessage];
    }
}

+ (NSManagedObject<XMPPUser> *)getConversationUserFromUserId:(NSString *)userId
{
    NSManagedObjectContext *moc = [[PHMessagingManager sharedPHMessagingManager] managedObjectContext_roster];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *tmpArrayObjects = [moc executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject <XMPPUser> *tmpObject in tmpArrayObjects)
    {
        NSArray *tmpArray = [tmpObject.jid.bare componentsSeparatedByString:@"@"];
        NSString *tmpStrId = tmpArray.count > 0 ? tmpArray[0] : @"";
//         NSLog(@"object: %@ -> tmpstrId: %@", tmpObject.jid.bare, tmpStrId);
        if ([tmpStrId isEqualToString:userId]) return tmpObject;
    }
    
    return nil;
}

@end
