//
//  PHMessageListDataSource.h
//  PHMessaging
//
//  Created by Ricol Wang on 25/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHMessageListDataSource <NSObject>

- (NSArray *)PHMessageListGetListOfMessages;

@end
