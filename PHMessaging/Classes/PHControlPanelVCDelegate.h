//
//  PHControlPanelVCDelegate.h
//  PHMessaging
//
//  Created by Ricol Wang on 24/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PHControlPanelViewController;

@protocol PHControlPanelVCDelegate <NSObject>

- (void)PHControlPanelVCMessageSelected:(PHControlPanelViewController *)vc andMessage:(NSString *)Message;

@end
