//
//  PHMessagingManager.h
//  PHMessaging
//
//  Created by Philology on 6/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import <CFNetwork/CFNetwork.h>
#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/DDTTYLogger.h>
#import <CocoaLumberjack/DDASLLogger.h>

#import "PHMessaging.h"
#import "GCDAsyncSocket.h"
#import "XMPP.h"
#import "XMPPReconnect.h"
#import "XMPPCapabilitiesCoreDataStorage.h"
#import "XMPPRosterCoreDataStorage.h"
#import "XMPPvCardAvatarModule.h"
#import "XMPPvCardCoreDataStorage.h"
#import "XMPPMessageArchiving.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "PHMessagingAvatarDelegate.h"
#import "PHMessagingContactNameDataSource.h"
#import "PHMessageListDataSource.h"
#import "PHMessagingAvatarDatasource.h"
#import "PHContactsVCDelegate.h"

@interface PHMessagingManager : NSObject <XMPPRosterDelegate, PHMessagingAvatarDelegate>

@property (weak) id <PHMessagingAvatarDelegate> delegateForAvatar;
@property (weak) id <PHMessagingContactNameDataSource> dataSourceForContactName;
@property (weak) id <PHMessageListDataSource> dataSourceForMessageList;
@property (weak) id <PHMessagingAvatarDatasource> dataSourceForAvatar;
@property (weak) id <PHContactsVCDelegate> delegateForContactVC;

@property (strong) XMPPStream *xmppStream;
@property (strong) XMPPReconnect *xmppReconnect;
@property (strong) XMPPRoster *xmppRoster;
@property (strong) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property (strong) XMPPvCardCoreDataStorage *xmppvCardStorage;
@property (strong) XMPPvCardTempModule *xmppvCardTempModule;
@property (strong) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (strong) XMPPCapabilities *xmppCapabilities;
@property (strong) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
@property (strong) XMPPMessageArchiving *xmppMessageArchiving;
@property (strong) XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingStorage;

@property (copy) NSString *hostname;
@property (copy) NSNumber *port;

- (NSManagedObjectContext *)managedObjectContext_roster;
- (NSManagedObjectContext *)managedObjectContext_capabilities;
- (NSManagedObjectContext *)managedObjectContext_messages;

- (void)handleUIApplicationDidFinishLaunchingNotification;
- (void)disconnect;
- (void)AddDelegate:(id <XMPPStreamDelegate>) delegate;
- (void)RemoveDelegate:(id <XMPPStreamDelegate>) delegate;
- (BOOL)connect;
- (BOOL)connectWithUser:(NSString*)userJID password:(NSString*)password;
- (NSString *)getUserNameForUserId:(XMPPJID *)userId;
- (UIImage *)getOnLineAvatarForUserId:(NSString *)userId;
- (UIImage *)getOffLineAvatarForUserId:(NSString *)userId;
- (NSString *)getUserNameForOldName:(NSString *)oldName;
- (NSArray *)getMessageList;
+ (void)broadcaseMessage:(NSString *)message;
+ (NSManagedObject <XMPPUser> *)getConversationUserFromUserId:(NSString *)userId;

+ (PHMessagingManager *)sharedPHMessagingManager;

@end
