//
//  PHBasicConversationViewController.m
//  PHMessaging
//
//  Created by Ricol Wang on 1/10/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicConversationViewController.h"
#import "PHMessagingManager.h"
#import "UIColor+JSMessagesView.h"
#import "PHMessagingSplitViewController.h"
#import <JSMessagesViewController/JSMessageSoundEffect.h>

@interface PHBasicConversationViewController ()

@end

@implementation PHBasicConversationViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    NSArray *sections = [[self fetchedResultsController] sections];
    
    if ((NSUInteger)sectionIndex < [sections count])
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:sectionIndex];
        return sectionInfo.numberOfObjects;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self getHeightForString:[self getTextFromIndexPath:indexPath]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    [self.tableViewMain reloadData];
    
    [self scrollToBottom:YES];
    
    [JSMessageSoundEffect playMessageReceivedSound];
}

#pragma mark - Override Methods

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController == nil)
    {
        XMPPMessageArchivingCoreDataStorage *storage = [[PHMessagingManager sharedPHMessagingManager] xmppMessageArchivingStorage];
        
        NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
        NSEntityDescription *entity = [storage messageEntity:moc];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
        
        PHMessagingManager *tmpMessagingManager = [PHMessagingManager sharedPHMessagingManager];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@ AND streamBareJidStr == %@", self.targetUser.jid.bare, tmpMessagingManager.xmppStream.myJID.bare];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setFetchBatchSize:10];
        
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:moc
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
        [_fetchedResultsController setDelegate:self];
        
        
        NSError *error = nil;
        if (![_fetchedResultsController performFetch:&error])
        {
            NSLog(@"Error performing fetch: %@", error);
        }
    }
    
    return _fetchedResultsController;
}

#pragma mark - Private Methods

- (NSString *)getTextFromIndexPath:(NSIndexPath *)indexPath
{
    XMPPMessageArchiving_Message_CoreDataObject *tmpMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([tmpMessage isKindOfClass:[XMPPMessageArchiving_Message_CoreDataObject class]])
        return tmpMessage.body;
    else
        return nil;
}

#pragma mark - Public Methods

- (float)getHeightForString:(NSString *)string
{
    float heightRow = 76 + 28;
    
    if (string)
        heightRow += [JSBubbleView neededHeightForText:string] - 50;
    
    return heightRow;
}

- (void)initialize
{
    [[JSBubbleView appearance] setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    
    self.dfMessageDate = [[NSDateFormatter alloc] init];
    self.dfMessageDate.dateFormat = @"HH:mm:ss dd/MM/yyyy";
}

- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self.fetchedResultsController objectAtIndexPath:indexPath] isOutgoing])
    {
        return JSBubbleMessageTypeOutgoing;
    }
    else
    {
        return JSBubbleMessageTypeIncoming;
    }
}

- (UIImageView *)bubbleImageViewWithType:(JSBubbleMessageType)type
                       forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self.fetchedResultsController objectAtIndexPath:indexPath] isOutgoing])
    {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type
                                                          color:[UIColor js_bubbleGreenColor]];
    }
    else
    {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type
                                                          color:[UIColor js_bubbleBlueColor]];
    }
}

- (void)scrollToTop:(BOOL)animate
{
    [self.tableViewMain scrollRectToVisible:CGRectMake(0, 0, self.tableViewMain.frame.size.width, self.tableViewMain.frame.size.height) animated:animate];
}

- (void)scrollToBottom:(BOOL)animate
{
    CGSize size = self.tableViewMain.contentSize;
    CGRect tmpRect = CGRectZero;
    tmpRect.size = size;
    if(tmpRect.size.height > self.view.bounds.size.height)
    {
        tmpRect.origin.y = tmpRect.size.height - self.view.bounds.size.height;
        [self.tableViewMain scrollRectToVisible:tmpRect animated:animate];
    }
}

- (UIImage *)avatarImageForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *tmpImage = nil;
    
    XMPPMessageArchiving_Message_CoreDataObject *tmpMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //    NSLog(@"%@ - %@: (%d, %d) tmpMessage.From: %@, tmpMessage.To: %@", THIS_FILE, THIS_METHOD, indexPath.section, indexPath.row, tmpMessage.message.from.bare, tmpMessage.message.to.bare);
    
    NSString *tmpStr = nil;
    
    if (tmpMessage.isOutgoing)
    {
        tmpStr = [[[PHMessagingManager sharedPHMessagingManager] xmppStream] myJID].bare;
        NSString *tmpStrUserId = tmpStr;
        NSArray *tmpArray = [tmpStrUserId componentsSeparatedByString:@"_"];
        
        if (![PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"])
            [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"] = [UIImage imageNamed:@"PersonOnLine"];
        
        tmpImage = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"];
        
        if (tmpArray.count > 0)
        {
            NSString *tmpStrKeyOnLine = [NSString stringWithFormat:@"%@OnLine", tmpArray[0]];
            
            UIImage *tmpImageOnLineAvatar = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOnLine];
            if (!tmpImageOnLineAvatar)
            {
                tmpImageOnLineAvatar = [[PHMessagingManager sharedPHMessagingManager] getOnLineAvatarForUserId:tmpArray[0]];
                if (tmpImageOnLineAvatar)
                    [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOnLine] = tmpImageOnLineAvatar;
            }
            
            if (tmpImageOnLineAvatar) tmpImage = tmpImageOnLineAvatar;
        }
    }
    else
    {
        tmpStr = self.targetUser.jid.bare;
        NSString *tmpStrUserId = tmpStr;
        NSArray *tmpArray = [tmpStrUserId componentsSeparatedByString:@"_"];
        
        if (self.targetUser.isOnline)
        {
            if (![PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"])
                [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"] = [UIImage imageNamed:@"PersonOnLine"];
            
            tmpImage = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"];
            
            if (tmpArray.count > 0)
            {
                NSString *tmpStrKeyOnLine = [NSString stringWithFormat:@"%@OnLine", tmpArray[0]];
                
                UIImage *tmpImageOnLineAvatar = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOnLine];
                if (!tmpImageOnLineAvatar)
                {
                    tmpImageOnLineAvatar = [[PHMessagingManager sharedPHMessagingManager] getOnLineAvatarForUserId:tmpArray[0]];
                    if (tmpImageOnLineAvatar)
                        [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOnLine] = tmpImageOnLineAvatar;
                }
                
                if (tmpImageOnLineAvatar) tmpImage = tmpImageOnLineAvatar;
            }
        }else
        {
            if (![PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOffLine"])
                [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOffLine"] = [UIImage imageNamed:@"PersonOffLine"];
            
            tmpImage = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOffLine"];
            
            if (tmpArray.count > 0)
            {
                NSString *tmpStrKeyOffLine = [NSString stringWithFormat:@"%@OffLine", tmpArray[0]];
                
                UIImage *tmpImageOffLineAvatar = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOffLine];
                if (!tmpImageOffLineAvatar)
                {
                    tmpImageOffLineAvatar = [[PHMessagingManager sharedPHMessagingManager] getOffLineAvatarForUserId:tmpArray[0]];
                    if (tmpImageOffLineAvatar)
                        [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOffLine] = tmpImageOffLineAvatar;
                }
                
                if (tmpImageOffLineAvatar) tmpImage = tmpImageOffLineAvatar;
            }
        }
    }
    
    return tmpImage;
}

@end
