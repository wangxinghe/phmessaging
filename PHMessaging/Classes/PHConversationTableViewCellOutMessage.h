//
//  PHConversationTableViewCellOutMessage.h
//  PHMessaging
//
//  Created by Ricol Wang on 24/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicConversationTableViewCellMessage.h"

@interface PHConversationTableViewCellOutMessage : PHBasicConversationTableViewCellMessage

@end
