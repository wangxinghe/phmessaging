//
//  PHBasicConversationTableViewCellMessage.m
//  PHMessaging
//
//  Created by Ricol Wang on 24/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHBasicConversationTableViewCellMessage.h"
#import "PHMessaging.h"

@interface PHBasicConversationTableViewCellMessage ()

@property (strong) UITapGestureRecognizer *tap;

@end

@implementation PHBasicConversationTableViewCellMessage

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.viewMyContent.frame = self.bounds;
    [self.contentView addSubview:self.viewMyContent];
    
#ifndef _DEBUG
    self.lblName.backgroundColor = [UIColor clearColor];
    self.lblDate.backgroundColor = [UIColor clearColor];
    self.tvMessage.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.viewMyContent.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.imageViewProfile.backgroundColor = [UIColor clearColor];
    self.imageViewBubble.backgroundColor = [UIColor clearColor];
    self.viewContainer.backgroundColor = [UIColor clearColor];
#else
    self.lblName.backgroundColor = [utils randomColor];
    self.lblDate.backgroundColor = [utils randomColor];
    self.tvMessage.backgroundColor = [utils randomColor];
    self.backgroundColor = [utils randomColor];
    self.viewMyContent.backgroundColor = [utils randomColor];
    self.contentView.backgroundColor = [utils randomColor];
    self.imageViewProfile.backgroundColor = [utils randomColor];
    self.imageViewBubble.backgroundColor = [utils randomColor];
    self.viewContainer.backgroundColor = [utils randomColor];
#endif
    
    if (!self.tap) self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    
    [self removeGestureRecognizer:self.tap];
    [self addGestureRecognizer:self.tap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tap
{
    NSLog(@"text: \n=====\n%@\n=====.", self.tvMessage.text);
}

@end
