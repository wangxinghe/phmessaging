//
//  PHContactListViewController.m
//  PHMessaging
//
//  Created by Ricol Wang on 20/03/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHContactListViewController.h"
#import "PHMessagingManager.h"
#import "PHBroadcastViewController.h"
#import <PHCommon/utils.h>

@interface PHContactListViewController ()

{
    BOOL bSelectAll;
    BOOL bAlreadyRun;
}

@property (strong) UIBarButtonItem *barBtnItemClose;
@property (strong) UIBarButtonItem *barBtnItemNext;
@property (strong) UIBarButtonItem *barBtnItemSelectAll;
@property (strong) NSMutableDictionary *mutableDict;

@end

@implementation PHContactListViewController

#pragma mark - View Life Cycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!bAlreadyRun)
    {
        self.title = PHMessagingString(@"SelectContacts");
        self.viewTop.hidden = YES;
        self.tableView.frame = self.view.bounds;
        
        if ([utils isIOS7])
            self.edgesForExtendedLayout = UIRectEdgeNone;
        
        self.mutableDict = [NSMutableDictionary new];
        
        self.barBtnItemClose = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCloseOnTapped:)];
        self.navigationItem.leftBarButtonItem = self.barBtnItemClose;
        
        self.barBtnItemSelectAll = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"  %@  ", PHMessagingString(@"All")] style:UIBarButtonItemStylePlain target:self action:@selector(btnAllOnTapped:)];
        
        self.barBtnItemNext = [[UIBarButtonItem alloc] initWithTitle:PHMessagingString(@"Next") style:UIBarButtonItemStylePlain target:self action:@selector(btnNextOnTapped:)];

        self.navigationItem.rightBarButtonItems = @[self.barBtnItemNext, self.barBtnItemSelectAll];
        
        [self validateBtn];
        
        bAlreadyRun = YES;
    }
}

#pragma mark - Override Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *tmpCell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    XMPPUserCoreDataStorageObject *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *tmpStrID = user.jid.bare;
    
    if (!self.mutableDict[tmpStrID])
        self.mutableDict[tmpStrID] = @(0);
    
    tmpCell.accessoryType = [self.mutableDict[tmpStrID] intValue] == 0 ? UITableViewCellAccessoryNone : UITableViewCellAccessoryCheckmark;
    tmpCell.detailTextLabel.text = @"";

    return tmpCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XMPPUserCoreDataStorageObject *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *tmpStrID = user.jid.bare;
    
    if (!self.mutableDict[tmpStrID])
        self.mutableDict[tmpStrID] = @(0);
    
    int number = [self.mutableDict[tmpStrID] intValue];
    number = number == 0 ? 1: 0;
    self.mutableDict[tmpStrID] = @(number);
    
    UITableViewCell *tmpCell = [self.tableView cellForRowAtIndexPath:indexPath];
    tmpCell.accessoryType = number == 0 ? UITableViewCellAccessoryNone : UITableViewCellAccessoryCheckmark;
    
    [self validateBtn];
}

#pragma mark - Private Methods

- (void)validateBtn
{
    BOOL bSelected = NO;
    
    NSArray *tmpArrayAllIDs = self.fetchedResultsController.fetchedObjects;
    for (XMPPUserCoreDataStorageObject *user in tmpArrayAllIDs)
    {
        NSString *tmpStrID = user.jid.bare;
        if ([self.mutableDict[tmpStrID] intValue] == 1)
        {
            bSelected = YES;
            break;
        }
    }
    
    self.barBtnItemNext.enabled = bSelected;
}

- (void)btnCloseOnTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)btnAllOnTapped:(id)sender
{
    bSelectAll = !bSelectAll;
    self.barBtnItemSelectAll.title = bSelectAll ? [NSString stringWithFormat:@"  %@  ", PHMessagingString(@"None")] : [NSString stringWithFormat:@"  %@  ", PHMessagingString(@"All")];
    
    NSArray *tmpArrayAllIDs = self.fetchedResultsController.fetchedObjects;
    for (XMPPUserCoreDataStorageObject *user in tmpArrayAllIDs)
    {
        NSString *tmpStrID = user.jid.bare;
        self.mutableDict[tmpStrID] = bSelectAll ? @(1) : @(0);
    }
    
    [self.tableView reloadData];
    
    [self validateBtn];
}

- (void)btnNextOnTapped:(id)sender
{
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    NSArray *tmpArrayAllObjects = [self.fetchedResultsController fetchedObjects];
    
    for (XMPPUserCoreDataStorageObject *tmpObject in tmpArrayAllObjects)
    {
        NSString *tmpStrId = tmpObject.jid.bare;
        if ([self.mutableDict[tmpStrId] intValue] == 1) [tmpMutableArray addObject:tmpObject.jid];
    }
    
    PHBroadcastViewController *tmpBroadcastVC = [[PHBroadcastViewController alloc] initWithUsers:tmpMutableArray];
    [self.navigationController pushViewController:tmpBroadcastVC animated:YES];
}

#pragma mark - Public Methods

- (void)selectUser:(NSArray *)users
{
    NSLog(@"Contact List: Select users: %@", users);
    
    for (NSString *tmpStrUserDisplayName in users)
    {
        if ([tmpStrUserDisplayName isEqualToString:@""]) continue;
        
        self.mutableDict[tmpStrUserDisplayName] = @(1);
        
        [self.tableView reloadData];
        
        [self validateBtn];
    }
}

@end
