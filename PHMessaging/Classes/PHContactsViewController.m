//
//  PHContactsViewController.m
//  PHMessaging
//
//  Created by Philology on 4/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHContactsViewController.h"
#import "PHConversationViewController.h"
#import "PHMessagingManager.h"
#import <PHCommon/utils.h>
#import "PHMessagingSplitViewController.h"
#import "PHContactListViewController.h"

@interface PHContactsViewController () <XMPPStreamDelegate, UITableViewDelegate>

{
    BOOL bAlreadyRun;
}

@property (weak) PHConversationViewController *conversationVC;

@property (strong) NSMutableDictionary *mutableDictCount;
@property (strong) UIBarButtonItem *barBtnItemClose;
@property (strong) UIBarButtonItem *barBtnItemBroadcast;

@end

@implementation PHContactsViewController

#pragma mark - View Life Cycle

- (instancetype)init
{
    self = [super initWithNibName:@"PHContactsViewController" bundle:nil];
    if (self)
    {

    }
    return self;
}

- (instancetype)initWithConversation:(PHConversationViewController *)conversation
{
    self = [super initWithNibName:@"PHContactsViewController" bundle:nil];
    if (self)
    {
        self.conversationVC = conversation;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if ([utils isIOS7])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.mutableDictCount = [NSMutableDictionary new];
    
    if ([utils isIPhone])
    {
        self.viewTop.hidden = YES;
        self.tableView.frame = self.view.bounds;
        
        self.barBtnItemBroadcast = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Team"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBroadcastOnTapped:)];
        self.navigationItem.rightBarButtonItem = self.barBtnItemBroadcast;
        
        self.barBtnItemClose = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCloseOnTapped:)];
        self.navigationItem.leftBarButtonItem = self.barBtnItemClose;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [[PHMessagingManager sharedPHMessagingManager] AddDelegate:self];
    [[[PHMessagingManager sharedPHMessagingManager] xmppvCardTempModule] addDelegate:self delegateQueue:dispatch_get_main_queue()];
	[[PHMessagingManager sharedPHMessagingManager] connect];
    
    if (!bAlreadyRun)
    {
#ifndef _DEBUG
        self.viewTop.backgroundColor = [UIColor clearColor];
#endif
        self.title = PHMessagingString(@"Contacts");
        self.lblTitle.text = PHMessagingString(@"Contacts");
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
        }
        
        bAlreadyRun = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[PHMessagingManager sharedPHMessagingManager] RemoveDelegate:self];
	//[[PHMessagingManager sharedPHMessagingManager] disconnect];
	[[[PHMessagingManager sharedPHMessagingManager] xmppvCardTempModule] removeDelegate:self];
	
	[super viewWillDisappear:animated];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    NSLog(@"anObject.class: %@", [anObject class]);
    
    UITableView *tableView = self.tableView;
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
        {
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            break;
        }
            
        case NSFetchedResultsChangeDelete:
        {
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
            break;
        }
            
        case NSFetchedResultsChangeUpdate:
        {
            @try
            {
                XMPPUserCoreDataStorageObject *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
                [self configurePhotoForCell:[tableView cellForRowAtIndexPath:indexPath] user:user andIndexPath:indexPath];
            }
            @catch (NSException *exception)
            {
                NSLog(@"exception: %@", exception.description);
            }
            @finally
            {
                ;
            }
            
            break;
        }
        case NSFetchedResultsChangeMove:
        {
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            break;
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    UITableView *tableView = self.tableView;
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[[self fetchedResultsController] sections] count];
}

- (NSString *)tableView:(UITableView *)sender titleForHeaderInSection:(NSInteger)sectionIndex
{
	NSArray *sections = [[self fetchedResultsController] sections];
	
	if (sectionIndex < (int)[sections count])
	{
		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:sectionIndex];
        
		int section = [sectionInfo.name intValue];
		switch (section)
		{
			case 0  : return [NSString stringWithFormat:@"%@ (%d)", PHMessagingString(@"Online"), (int)sectionInfo.numberOfObjects];
			case 1  : return [NSString stringWithFormat:@"%@ (%d)", PHMessagingString(@"Away"), (int)sectionInfo.numberOfObjects];
			default : return [NSString stringWithFormat:@"%@ (%d)", PHMessagingString(@"Offline"), (int)sectionInfo.numberOfObjects];
		}
	}
	
	return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
	NSArray *sections = [[self fetchedResultsController] sections];
	
	if (sectionIndex < (int)[sections count])
	{
		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:sectionIndex];
		return sectionInfo.numberOfObjects;
	}
	
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
	
	XMPPUserCoreDataStorageObject *user = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
	[self configurePhotoForCell:cell user:user andIndexPath:indexPath];
	
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    if ([object isKindOfClass:[XMPPUserCoreDataStorageObject class]])
    {
        XMPPUserCoreDataStorageObject *tmpObject = (XMPPUserCoreDataStorageObject *)object;
        NSString *tmpStrID = tmpObject.jid.bare;
        self.mutableDictCount[tmpStrID] = @(0);
    }
    
    if ([utils isIPhone])
    {
        PHConversationViewController *tmpVC = [[PHConversationViewController alloc] init];
        tmpVC.view.frame = self.view.bounds;
        [self.navigationController pushViewController:tmpVC animated:YES];
        
        [utils runAfter:0.1 andBlock:^(){
            tmpVC.targetUser = (NSManagedObject<XMPPUser>*)object;
        }];
    }else
    {
        self.conversationVC.targetUser = (NSManagedObject<XMPPUser>*)object;
        UITableViewCell *tmpCell = [self.tableView cellForRowAtIndexPath:indexPath];
        tmpCell.detailTextLabel.text = @"";
    }
}

#pragma mark - Override Methods

- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController == nil)
	{
		NSManagedObjectContext *moc = [[PHMessagingManager sharedPHMessagingManager] managedObjectContext_roster];
		
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
		                                          inManagedObjectContext:moc];
        
		NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"sectionNum" ascending:YES];
		NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"displayName" ascending:YES];
		
		NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
		
		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		[fetchRequest setEntity:entity];
		[fetchRequest setSortDescriptors:sortDescriptors];
		[fetchRequest setFetchBatchSize:10];
        
		_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:moc
                                                                          sectionNameKeyPath:@"sectionNum"
                                                                                   cacheName:nil];
		[_fetchedResultsController setDelegate:self];
		
		NSError *error = nil;
		if (![_fetchedResultsController performFetch:&error])
		{
			NSLog(@"Error performing fetch: %@", error);
		}
	}
	
	return _fetchedResultsController;
}

#pragma mark - Private Methods

- (void)configurePhotoForCell:(UITableViewCell *)cell user:(XMPPUserCoreDataStorageObject *)user andIndexPath:(NSIndexPath *)indexPath
{
	// Our xmppRosterStorage will cache photos as they arrive from the xmppvCardAvatarModule.
	// We only need to ask the avatar module for a photo, if the roster doesn't have it.
	
//    NSLog(@"%@ - %@: user: %@", THIS_FILE, THIS_METHOD, user);
    
    UIImage *tmpImage = nil;
    NSString *tmpStrUserId = user.jid.bare;
    NSArray *tmpArray = [tmpStrUserId componentsSeparatedByString:@"_"];
    
    if (user.isOnline)
    {
        if (![PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"])
            [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"] = [UIImage imageNamed:@"PersonOnLine"];
        
        tmpImage = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOnLine"];
        
        if (tmpArray.count > 0)
        {
            NSString *tmpStrKeyOnLine = [NSString stringWithFormat:@"%@OnLine", tmpArray[0]];
            
            UIImage *tmpImageOnLineAvatar = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOnLine];
            if (!tmpImageOnLineAvatar)
            {
                tmpImageOnLineAvatar = [[PHMessagingManager sharedPHMessagingManager] getOnLineAvatarForUserId:tmpArray[0]];
                if (tmpImageOnLineAvatar)
                    [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOnLine] = tmpImageOnLineAvatar;
            }
            
            if (tmpImageOnLineAvatar) tmpImage = tmpImageOnLineAvatar;
        }
    }else
    {
        if (![PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOffLine"])
            [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOffLine"] = [UIImage imageNamed:@"PersonOffLine"];
        
        tmpImage = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[@"PersonOffLine"];
        
        if (tmpArray.count > 0)
        {
            NSString *tmpStrKeyOffLine = [NSString stringWithFormat:@"%@OffLine", tmpArray[0]];
            
            UIImage *tmpImageOffLineAvatar = [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOffLine];
            if (!tmpImageOffLineAvatar)
            {
                tmpImageOffLineAvatar = [[PHMessagingManager sharedPHMessagingManager] getOffLineAvatarForUserId:tmpArray[0]];
                if (tmpImageOffLineAvatar)
                    [PHMessagingManager sharedPHMessagingManager].dataSourceForAvatar.mutableDictAvatars[tmpStrKeyOffLine] = tmpImageOffLineAvatar;
            }
            
            if (tmpImageOffLineAvatar) tmpImage = tmpImageOffLineAvatar;
        }
    }

    cell.imageView.image = tmpImage;
//    NSLog(@"cell.imageView: %@ andImage: %@", cell.imageView, tmpImage);
    cell.textLabel.text = [[PHMessagingManager sharedPHMessagingManager] getUserNameForOldName:user.displayName];
    
    NSString *tmpStrID = user.jid.bare;
    int count = [self.mutableDictCount[tmpStrID] intValue];
    if (count > 0)
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", count];
    else
        cell.detailTextLabel.text = @"";
}

#pragma mark - XMPPStreamDelegate

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    NSString *tmpStrFromID = message.from.bare;
    int count = [self.mutableDictCount[tmpStrFromID] intValue];
    count++;
    self.mutableDictCount[tmpStrFromID] = @(count);
    
    NSIndexPath *tmpIndexPath = nil;
    NSArray *tmpArray = self.fetchedResultsController.fetchedObjects;
    for (XMPPUserCoreDataStorageObject *tmpObject in tmpArray)
    {
        NSString *tmpStrID = tmpObject.jid.bare;
        if ([tmpStrID isEqualToString:tmpStrFromID])
        {
            tmpIndexPath = [self.fetchedResultsController indexPathForObject:tmpObject];
            if (tmpIndexPath)
                [self.tableView reloadRowsAtIndexPaths:@[tmpIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
//    NSLog(@"Presence: from: %@, to: %@", presence.fromStr, presence.toStr);
//    
//    if ([presence.from isEqualToJID:self.parent.conversationVC.targetUser.jid])
//    {
//        for (NSManagedObject *tmpObject in self.fetchedResultsController.fetchedObjects)
//        {
//            if ([tmpObject conformsToProtocol:@protocol(XMPPUser)])
//            {
//                if ([((NSManagedObject<XMPPUser> *)tmpObject).jid isEqualToJID:presence.from])
//                {
//                    self.parent.conversationVC.targetUser = (NSManagedObject<XMPPUser>*)tmpObject;
//                    break;
//                }
//            }
//        }
//    }
}

#pragma mark - Private Methods

- (void)btnBroadcastOnTapped:(id)sender
{
    PHContactListViewController *tmpVC = [[PHContactListViewController alloc] initWithConversation:self.conversationVC];
    
    UINavigationController *tmpNav = [[UINavigationController alloc] initWithRootViewController:tmpVC];
    tmpNav.modalPresentationStyle = UIModalPresentationFormSheet;
    if ([utils isIOS8])
        tmpNav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    else
        tmpNav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:tmpNav animated:YES completion:nil];
}

- (void)btnCloseOnTapped:(id)sender
{
    [[PHMessagingManager sharedPHMessagingManager].delegateForContactVC PHContactsVCQuit:self];
}

@end
