//
//  PHControlPanelViewController.m
//  PHMessaging
//
//  Created by Ricol Wang on 22/09/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHControlPanelViewController.h"
#import <PHCommon/InforDialog.h>
#import "PHMessaging.h"
#import "PHMessagingManager.h"

@interface PHControlPanelViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;

@property (strong) NSArray *arrayMessages;

@end

@implementation PHControlPanelViewController

#pragma mark - View Life Cycle

- (instancetype)init
{
    self = [super initWithNibName:@"PHControlPanelViewController" bundle:nil];
    
    if (self)
    {
        self.arrayMessages = [[PHMessagingManager sharedPHMessagingManager] getMessageList];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = PHMessagingString(@"MessageList");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMessages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tmpStrIdentifier = @"UITableViewCell_Identifier";
    
    UITableViewCell *tmpCell = [tableView dequeueReusableCellWithIdentifier:tmpStrIdentifier];
    
    if (!tmpCell)
    {
        tmpCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tmpStrIdentifier];
    }
    
    NSDictionary *tmpDict = self.arrayMessages[indexPath.row];
    tmpCell.textLabel.text = tmpDict[KEY_MESSAGE_LIST_TITLE];
    
    return tmpCell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tmpDict = self.arrayMessages[indexPath.row];
    [self.delegate PHControlPanelVCMessageSelected:self andMessage:tmpDict[KEY_MESSAGE_LIST_VALUE] ? tmpDict[KEY_MESSAGE_LIST_VALUE] : tmpDict[KEY_MESSAGE_LIST_TITLE]];
}

@end
